/*credits   http://atomicrobotdesign.com/blog/htmlcss/build-a-vertical-scrolling-shooter-game-with-html5-canvas-part-6/
*/

var canvas,
    ctx,
    width = 600,
    height = 600,
    enemyTotal = 5,
    enemies = [],
    enemy_x = 0,
    enemy_y = 0,
    enemy_w = 90,
    enemy_h = 90,
    speed = 2,
    enemy,
    rightKey = false,
    leftKey = false,
    upKey = false,
    downKey = false,
    zombie,
    zombie_x = (width / 2) - 25, zombie_y = height - 75, zombie_w = 50, zombie_h = 57,
    laserTotal = 2,
    lasers = [],
    score = 0,
    alive = true,
    lives = 3,
	level =1 ,
	kills = 0,
	bkg,
    bkgX = 0, bkgY = 0, bkgY2 = -600,
    gameStarted = false,
	laser_x= 50,
	laser_y = 50,
	sound = true,
	music = true;

/* ======================================================*/
function resetStats() { 
 score =0; lives =3; level =1; speed = 2;

}

function makeInput()
{
	var input = new CanvasInput({
  canvas: document.getElementById('canvas'),
  fontSize: 18,
  fontFamily: 'Arial',
  fontColor: '#212121',
  fontWeight: 'bold',
  width: 300,
  padding: 8,
  borderWidth: 1,
  borderColor: '#000',
  borderRadius: 3,
  boxShadow: '1px 1px 0px #fff',
  innerShadow: '0px 0px 5px rgba(0, 0, 0, 0.5)',
  placeHolder: 'Enter message here...'
});
}
//Draws the text for the score and lives on the canvas and if the player runs out of lives, it's draws the game over text and continue button as well as adding the event listener for the continue button
function scoreTotal() {
i
ctx.font = 'bold 22px Creepster';
 ctx.fillStyle = '#f8991d';
ctx.fillText('Level: ', 10, 30);
 ctx.fillStyle = '#fff';
ctx.fillText(level, 70, 30);
 ctx.fillStyle = '#f8991d';
ctx.fillText('Score: ', 10, 80);
 ctx.fillStyle = '#fff';
ctx.fillText(score, 70, 80);
 ctx.fillStyle = '#f8991d';
 ctx.fillText('Lives:', 10, 55);
  ctx.fillStyle = '#fff';
 ctx.fillText(lives, 68, 55);
 
  if (!gameStarted) {
  ctx.font = 'bold 50px Creepster';
   ctx.fillStyle = '#f8991d';
  ctx.fillText('Zombie Shooter', width / 2 - 110, height / 2);
   ctx.fillStyle = '#fff';
  ctx.font = 'bold 20px Creepster';
  ctx.fillText('Click to Play', width / 2 - 56, height / 2 + 30);
     ctx.fillStyle = ' #0d548a';

  ctx.fillText('Use arrow keys to move', width / 2 - 100, height / 2 + 60);
  ctx.fillText('Use the x key to shoot', width / 2 - 100, height / 2 + 90);
  
  
   
}
 if (!alive) {
  ctx.font = 'bold 50px Creepster';
   ctx.fillStyle = '#f8991d';
ctx.fillText('YOUR SCORE:', 85, height / 2+ 100);
   ctx.fillStyle = '#fff';
ctx.fillText(score, 335, height / 2+ 100);
  ctx.font = 'bold 20px Creepster';
   ctx.fillStyle = '#f8991d';
   ctx.fillText('Game Over!', 245, height / 2);
      ctx.fillStyle = '#fff';
   ctx.fillRect((width / 2) - 53, (height / 2) + 10,100,40);
   ctx.fillStyle = '#000';
   ctx.fillText('Play Again?', 252, (height / 2) + 35);
   canvas.addEventListener('click', continueButton, false);

 }
 

}
/*------------------------------------------------------------------------*/
function checkLives() {
 lives -= 1;
 score -=10;
 if (lives > 0) {
	 if(sound) ion.sound.play("zombie_die",{volume: 1.0});
   reset();
 } else if (lives == 0) {
   alive = false;
		if(sound){ ion.sound.stop("Toccata");
			ion.sound.play("died"); }
	  
 }
}
/*------------------------------------------------------------------------*/
function reset() {
 var enemy_reset_x = 50;
zombie_x = (width / 2) - 25,zombie_y = height - 75,zombie_w = 50,zombie_h = 57;
 for (var i = 0; i < enemies.length; i++) {
   enemies[i][0] = enemy_reset_x;
   enemies[i][1] = -45;
   enemy_reset_x = enemy_reset_x + enemy_w + randomRange(45,60, 1);
 }

}
/*------------------------------------------------------------------------*/
//After the player loses all their lives, the continue button is shown and if clicked, it resets the game and removes the event listener for the continue button
function continueButton(e) {
 var cursorPos = getCursorPos(e);
 if (cursorPos.x > (width / 2) - 53 && cursorPos.x < (width / 2) + 47 && cursorPos.y > (height / 2) + 10 && cursorPos.y < (height / 2) + 50) {
   alive = true;
   resetStats();
    if(sound && music)  ion.sound.play("Toccata",{loop:true});
    reset();
    canvas.removeEventListener('click', continueButton, false);
  }
}

//holds the cursors position
function cursorPosition(x,y) {
  this.x = x;
  this.y = y;
}

//finds the cursor's position after the mouse is clicked
function getCursorPos(e) {
  var x;
  var y;
  if (e.pageX || e.pageY) {
    x = e.pageX;
    y = e.pageY;
  } else {
    x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }
  x -= canvas.offsetLeft;
  y -= canvas.offsetTop;
  var cursorPos = new cursorPosition(x, y);
  return cursorPos;
}
/*------------------------------------------------------------------------*/
function drawBkg() {
  ctx.drawImage(bkg,bkgX,bkgY);
  ctx.drawImage(bkg,bkgX,bkgY2);
  if (bkgY > 1200) {
    bkgY = -1199;
  }
  if (bkgY2 > 1200) {
    bkgY2 = -1199;
  }
  bkgY += 1;
  bkgY2 += 1;
}
/*------------------------------------------------------------------------*/
	function drawLaser() {
  if (lasers.length)
    for (var i = 0; i < lasers.length; i++) {	
      //ctx.fillStyle = '#f00';
	  
      ctx.fillRect(lasers[i][0],lasers[i][1],lasers[i][2],lasers[i][3])
    }
	
}
/*------------------------------------------------------------------------*/
function moveLaser() {

  for (var i = 0; i < lasers.length; i++) {
    if (lasers[i][1] > -11) {
      lasers[i][1] -= 10;
	  
    } else if (lasers[i][1] < -10) {
      lasers.splice(i, 1);
	   
    }
  }
  
}
/*------------------------------------------------------------------------*/
function hitTest() {
  var remove = false;
  for (var i = 0; i < lasers.length; i++) {
    for (var j = 0; j < enemies.length; j++) {
      if (lasers[i][1] <= (enemies[j][1] + enemies[j][3]) && lasers[i][0] >= enemies[j][0] && lasers[i][0] <= (enemies[j][0] + enemies[j][2])) {
	   
        remove = true;
		
        enemies.splice(j, 1);
        enemies.push([(Math.random() * 500) + 50, -45, enemy_w, enemy_h, speed]);
		if(sound) ion.sound.play("hit", {volume: 0.3});
		
		// ctx.drawImage(laser, enemies[i][j]  ,  enemies[i][j], laser_x, laser_y);
		 
	
		 
      }
    }
    if (remove == true) {
	
      lasers.splice(i, 1);
	 
      remove = false;
	  if(level >1) score += (10 * Math.ceil(level/2));
	  else score +=10;
	  kills += 1; 
	  //  for each level  mult by 100 and give player additional life if score reached
	  if(kills % (level *10) == 0 ) 
		{  
			level +=1; 
			enemy = new Image();
			enemy.src = 'images/monsters_0'+randomRange(1,5,1)+'.png';  
			if(sound)ion.sound.play("tada",{volume: "0.5"}); 
			speed +=  level / 6;
		}
	  if(score % (level *100) == 0 & score > 0) { if(sound) ion.sound.play("chaching"); lives +=1;}

    }
  }
}
/*------------------------------------------------------------------------*/	
function zombieCollision() {
  var zombie_xw = zombie_x + zombie_w,
      zombie_yh = zombie_y + zombie_h;
  for (var i = 0; i < enemies.length; i++) {
    if (zombie_x > enemies[i][0] && zombie_x < enemies[i][0] + enemy_w && zombie_y > enemies[i][1] && zombie_y < enemies[i][1] + enemy_h) {
      checkLives();

    }
    if (zombie_xw < enemies[i][0] + enemy_w && zombie_xw > enemies[i][0] && zombie_y > enemies[i][1] && zombie_y < enemies[i][1] + enemy_h) {
      checkLives();

    }
    if (zombie_yh > enemies[i][1] && zombie_yh < enemies[i][1] + enemy_h && zombie_x > enemies[i][0] && zombie_x < enemies[i][0] + enemy_w) {
      checkLives();

    }
    if (zombie_yh > enemies[i][1] && zombie_yh < enemies[i][1] + enemy_h && zombie_xw < enemies[i][0] + enemy_w && zombie_xw > enemies[i][0]) {
      checkLives();

    }
  }
  //if(!alive) score -=10;

}
/*------------------------------------------------------------------------*/	
//Array to hold all the enemies on screen

	for (var i = 0; i < enemyTotal; i++) {
  enemies.push([enemy_x, enemy_y, enemy_w, enemy_h, speed]);
  enemy_x += enemy_w + 60;//randomRange(45,60, 1);
    
}

/*-------------------------------*/	
//Cycles through the array and draws the updated enemy position
function drawEnemies() {

  for (var i = 0; i < enemies.length; i++) {
	
		


		/*var sourceX = enemies[i][0];;
        var sourceY = enemies[i][1]
        var sourceWidth = enemies[i][2]
        var sourceHeight = enemies[i][3]
        var destWidth = sourceWidth;
        var destHeight = sourceHeight;
        var destX = canvas.width / 2 - destWidth / 2;
        var destY = canvas.height / 2 - destHeight / 2;*/
        
		
		//ctx.drawImage(enemy, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
		//ctx.drawImage(enemy, sourceX, sourceY, sourceWidth, sourceHeight, destY, destY, destWidth, destHeight);
 ctx.drawImage(enemy, enemies[i][0], enemies[i][1] );

  }
}
/*------------------------------------------------------------------------*/
function moveEnemies() {
  for (var i = 0; i < enemies.length; i++) {
    if (enemies[i][1] < height) {
      enemies[i][1] += enemies[i][4];
    } else if (enemies[i][1] > height - 1) {
      enemies[i][1] = -45;
    }
  }
}
	/*------------------------------------------------------------------------*/
	function clearCanvas() {
  ctx.clearRect(0,0,width,height);
}
/*------------------------------------------------------------------------*/
function drawZombie() {
var zspeed = (5+level);
  if (rightKey) zombie_x += zspeed;
  else if (leftKey) zombie_x -= zspeed;
  if (upKey) zombie_y -= zspeed;
  else if (downKey) zombie_y += zspeed;
  if (zombie_x <= 0) zombie_x = 0;
  if ((zombie_x + zombie_w) >= width) zombie_x = width - zombie_w;
  if (zombie_y <= 0) zombie_y = 0;
  if ((zombie_y + zombie_h) >= height) zombie_y = height - zombie_h;
ctx.drawImage(zombie, zombie_x, zombie_y);

}
/*------------------------------------------------------------------------*/
function gameStart() {
  gameStarted = true;
  canvas.removeEventListener('click', gameStart, true);
  reset();
}
/*------------------------------------------------------------------------*/
//The initial function called when the page first loads. Loads the zombie, enemy and bkg images and adds the event listeners for the arrow keys. It then calls the gameLoop function.
function init() {

 canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');
 enemy = new Image();
enemy.src = 'images/monsters_0'+randomRange(1,5,1)+'.png';
  laser = new Image();
  laser.src = 'images/vomit.png';
  zombie = new Image();
  zombie.src = 'images/zombie_single.png';
  setInterval(gameLoop, 25);
  document.addEventListener('keydown', keyDown, false);
  document.addEventListener('keyup', keyUp, false);
  document.addEventListener("mousedown",doMouseDown, false);
 // game = setTimeout(gameLoop, 1000 / 30);
 bkg = new Image();
bkg.src = 'images/bkg.jpg';

canvas.addEventListener('click', gameStart, false);

		gameLoop();

}
/*------------------------------------------------------------------------*/
function gameStart() {
  gameStarted = true;
  canvas.removeEventListener('click', gameStart, false);

  if(sound && music) ion.sound.play("Toccata",{loop:true});
}

/*------------------------------------------------------------------------*/
//The main function of the game,
function gameLoop() {
 clearCanvas();

if (alive && gameStarted && lives > 0) {

 drawBkg();

   hitTest();
    zombieCollision();
    moveLaser();
    moveEnemies();
    drawEnemies();
    drawZombie();
    drawLaser();
  }
  scoreTotal();
  //game = setTimeout(gameLoop, 1000 / 30);
}
/*------------------------------------------------------------------------*/
//detect mouse click 
function doMouseDown(event){
		if(lasers.length <= laserTotal) {
  if(sound)ion.sound.play("bite");	
	lasers.push([zombie_x + 25, zombie_y - 20, 4, 20]);
	}
			
}
/*------------------------------------------------------------------------*/
//Checks to see which key has been pressed
function keyDown(e) {
  if (e.keyCode == 39 | e.keyCode == 68 ) rightKey = true;
  else if (e.keyCode == 37 | e.keyCode == 65) leftKey = true;
  if (e.keyCode == 38 | e.keyCode == 87) upKey = true;
  else if (e.keyCode == 40 | e.keyCode == 83) downKey = true;
  	if ((e.keyCode == 88 | e.keyCode ==32) && lasers.length <= laserTotal) {
  if(sound)ion.sound.play("bite");	
	lasers.push([zombie_x + 25, zombie_y - 20, 4, 20]);
	}
}
/*------------------------------------------------------------------------*/
//Checks to see if a pressed key has been released
function keyUp(e) {
  if (e.keyCode == 39  | e.keyCode == 68 ) rightKey = false;
  else if (e.keyCode == 37 | e.keyCode == 65 ) leftKey = false;
  if (e.keyCode == 38 | e.keyCode == 87) upKey = false;
  else if (e.keyCode == 40 | e.keyCode == 83) downKey = false;
}
/*------------------------------------------------------------------------*/
window.onload = init;