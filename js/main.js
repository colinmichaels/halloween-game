var timer = setInterval(flash, randomRange(1,10,1000));
var debug = false;
/* ----------------------------------------------*/
function db(txt)
{
	if(debug) console.log(txt);
}

function flash()
{
		if(sound)
		{
		var snd_rnd = randomRange(0,3,1);
		var sounds = ["lighting_sound","lighting_sound_2","lighting_sound_3"];
		db(snd_rnd);
		
		ion.sound.play(sounds[snd_rnd]);
		}
		//var pause = random(50,200, 1);
		$('#overlay').css("background-image", "url(images/lightining.png)");
		setTimeout(function() {
					$('#overlay').css("background-image", "none"); 
			 }, 400);
		clearInterval(timer);
       timer = setInterval(flash, randomRange(1,20,1000));
	   
	   // display a random term
	   var webterms = ["Responsive","WhiteSpace","CSS","HTML5","Adaptive","Mobile","Parallax Scrolling","S.E.O"];
	   var rand_term = randomRange(0,webterms.length,1);
	 
	   $("#boo").fadeOut().html(webterms[rand_term]);
		moveDiv("boo");
}

/* ----------------------------------------------*/
function randomRange(min,max,mult)
{
return delay = Math.floor(Math.random() * (max - min) + min) * mult;
}
/* ----------------------------------------------*/
function moveDiv(div) {
    var $span = $("#"+div);
    randomAnimation("bats");
    $span.fadeOut(1000, function() {
        var maxLeft = $(window).width() - $span.width();
        var maxTop = $(window).height() - $span.height();
        var leftPos = Math.floor(Math.random() * (maxLeft + 1));
        var topPos = Math.floor(Math.random() * (maxTop + 1));
     
        $span.css({ left: leftPos, top: topPos }).fadeIn(1000);
    });
};
/* ----------------------------------------------*/


function randomAnimation(div){
var $id = "#"+div;
$($id).fadeIn(1000);
 var maxLeft = $(window).width() - $($id).width();
        var maxTop = $(window).height() - $($id).height();
var animationDurration = 5000;
var leftPos = Math.floor(Math.random() * (maxLeft + 1));
var topPos = Math.floor(Math.random() * (maxTop + 1));
db("top=" + topPos+ " lef= "+leftPos);
  $($id).animate({
     left: leftPos,
     top: topPos
   }, animationDurration, function() {
     randomAnimation();
   });
  }

/* ----------------------------------------------*/
function makeBlood(src,type)
{
	var oldSrc = $(src).html();
	$(src).stop().html("<img src='images/bloody.png' />");
	$(src).stop().fadeOut(1000, function(){
		$(src).html(oldSrc);
	});
	db(oldSrc);
	
	
}
/* ----------------------------------------------*/
function addScore(amt)
{
	if(gameStarted && alive) 
	{
		score += amt;
	}
}

ion.sound({
    sounds: [
        { name: "lighting_sound"},
         {name: "lighting_sound_2" },
		 {name: "lighting_sound_3" },
		 { name: "Toccata"},
		{name: "mouse1"},
		{name: "died"},
		{name: "bite"},
		{name: "hit"},
		{name: "zombie_die"},
		{name: "chaching"},
		{name: "tada"}
    ],
    volume: 0.7,
    path: "sounds/",
    preload: true
});
/* ----------------------------------------------*/
/* =============================================================================*/
/* =============================================================================*/

$(document).ready(function() { 

//ion.sound.play("Toccata");
/*
$('body, html').click(function(e){
		event.preventDefault();  // ignore clicks on screen
});*/

 $('body').mousemove(function(e){
        var x = -(e.pageX + this.offsetLeft) / 10;
        var y = -(e.pageY + this.offsetTop) / 50;
        $(this).css('background-position', x + 'px ' + y + 'px');
    }); 

$('#boo').click(function(e){
     
		makeBlood(this);  // need to fix this.. 
		if(sound)ion.sound.play("bite");
		addScore(randomRange(25,100,1));
	
});	

$('#sound').click(function(e){
console.log("sound-" +sound);
	if(sound)
	{ sound = false; $(this).html("Sound On"); ion.sound.pause();}
	else { sound = true; $(this).html("Sound Off");}
});
$('#music').click(function(e){
	if(music)
	{ music =false; $(this).html("Music On"); ion.sound.pause("Toccata");}
	else { music = true; $(this).html("Music Off"); if(sound) ion.sound.play("Toccata",{loop:true});}
});

$("h1").click(function(){
	ion.sound.stop("Toccata");
});
$("#bats").click(function(){
		makeBlood(this);
		if(sound)ion.sound.play("mouse1");
		addScore(randomRange(25,100,1));
});


});

  