<?php session_start();
/* Author: Colin Michaels - Sept, 2014 for QuinnCom Web Design */
//error_reporting(E_ALL);
/* http://server.quinncom.net:2087/json-api/listaccts  */
include_once("includes/header.php"); ?>

<div id="trees">
<div id="overlay">

<div class="container">
<header>
<h1>Happy Halloween!</h1>
</header>

	<div id="content">
		<div id="left-side"><h3>Instructions</h3><hr /><?php include_once("includes/instructions.php");?></div>
		<canvas id="canvas" width="600" height="600"></canvas>
		<div id="right-side"><h3>High Scores</h3><hr /><div><?php include_once("includes/high_scores.php");?></div></div>
	</div>
</div>
</div>
</div>

<?php /*-----------------------------------------------------------------------------------------------*/
include_once("includes/footer.php"); ?>

